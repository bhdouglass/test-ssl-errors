/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "example.h"

Example::Example() {
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, &QNetworkAccessManager::sslErrors, [=](QNetworkReply *reply, QList<QSslError> errors) {
        qDebug() << "ssl errors" << errors[0].errorString() << errors[0].certificate();
    });
}

void Example::testProd() {
    QUrl url(QStringLiteral("https://open-store.io/api/v3/categories"));
    qDebug() << "Sending request to " << url;

    QNetworkReply *reply = m_manager->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error() == QNetworkReply::NoError) {
            qDebug() << "Got a reply" << reply->readAll();
        }
        else {
            qDebug() << "Got an error" << reply->errorString();
        }

        reply->deleteLater();
    });
}

void Example::testStaging() {
    QUrl url(QStringLiteral("https://staging.open-store.io/api/v3/categories"));
    qDebug() << "Sending request to " << url;

    QNetworkReply *reply = m_manager->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error() == QNetworkReply::NoError) {
            qDebug() << "Got a reply" << reply->readAll();
        }
        else {
            qDebug() << "Got an error" << reply->errorString();
        }

        reply->deleteLater();
    });
}
